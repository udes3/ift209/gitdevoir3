#.global main

/*
 * x19: Tableau accumulateur             x20: Tableau mémoire 4x2
 * x21: Code opération a effectuer       x22: Opérande
 * x27: Nombre colonnes de la mémoire    x28: Nombre d'octets dans 64 bits
*/

// ...
main:
	adr    x20,memoire           //Tableau mémoire tableau (4,2)
	adr    x19,acc               //Accumulateur tableau (2 éléments)
	mov    x28,16                //Nombre d'octet * Nombre de colonnes
// Lire le code d'opération
	adr    x0,fmtNum
	adr    x1,num
	bl     scanf
	ldr    x21,num                //Charge le code d'opération dans x21

//Cas code d'opération 5
Cas5:
	cmp    x21,5                  //Si code d'opé. == 5
	b.eq   Quitter                //Vrai:Quitter

// Lire l'opérande
	adr    x0,fmtNum
	adr    x1,num
	bl     scanf
	ldr    x22,num                //Charge l'opérande dans x22

//Cas code d'opération 0 : acc ← i
Cas0:
	cmp    x21,0                  //Si code d'opé. != 0
	b.ne   Cas1                   //Vrai:Passer au prochain cas

	mov    x27,0
	str    x27,[x19]              //Mettre l'acc à 0
	str    x22,[x19,8]            //Stocker i dans case (1) de l'acc
	b      AffichageAcc           //Aller à l'affichage

//Cas code d'opération 1 : mem[i] ← acc
Cas1:
	cmp    x21,1                  //Si code d'opé. != 1
	b.ne   Cas2                   //Vrai:Passer au prochain cas

	//Placement du pointeur x24  à (i,0) formule a+(i·n1+j)·k
	madd   x24,x22,x28,x20        //x20+i*16

	ldr    x23,[x19]              //Charger x23 avec Acc[0]
	str    x23,[x24]              //Stocker x23 dans mem[i]case 0

	ldr    x23,[x19,8]            //Charger x23 avec Acc[1]
	str    x23,[x24,8]            //Stocker x23 dans mem[i]case 1

	b      AffichageAcc           //Aller à l'affichage

//Cas code d'opération 2 : acc ← mem[i]
Cas2:
	cmp    x21,2                  //Si code d'opé. != 2
	b.ne   Cas3                   //Vrai:Passer au prochain cas

	//Placement du pointeur x24  à (i,0) formule a+(i·n1+j)·k
	madd   x24,x22,x28,x20        //x20+i*16

	ldr    x23,[x24]              //Charger x23 avec mem[i]case 0
	str    x23,[x19]              //Stocker x23 dans Acc[0]

	ldr    x23,[x24,8]            //Charger x23 avec mem[i]case 1
	str    x23,[x19,8]            //Stocker x23 dans Acc[1]

	b      AffichageAcc

//Cas code d'opération 3 : mem[i] ← mem[i] + acc
Cas3:
	cmp    x21,3                  //Si code d'opé. != 3
	b.ne   Cas4                   //Vrai:Passer au prochain cas

	//Chargement des 64 bits poid fort du acc dans x23
	ldr    x23,[x19]              //Acc
	//Placement du pointeur x24 (i,0) formule a+(i·n1+j)·k
	madd   x24,x22,x28,x20        //x20+i*16
	//Charger les bits à x24 (i,0) dans x25
	ldr    x25,[x24]              //Mémoire

	//Chargement des 64 bits poid faible du acc dans x26
	ldr    x26,[x19,8]            //Acc
	//Charger les bits à x24 (i,1) dans x27
	ldr    x27,[x24,8]            //Mémoire

	//Soustraction mem[i] + acc
	adds   x26,x27,x26            //Addition bits poid faible
	adcs   x23,x25,x23            //Addition bits poid fort + débordement

	str    x26,[x24,8]            //Stocker x26 dans mem[i]case 1
	str    x23,[x24]              //Stocker x23 dans mem[i]case 0
	b      AffichageAcc           //Aller à l'affichage

//Cas code d'opération 4 : mem[i] ← mem[i] − acc
Cas4:

	//Chargement des 64 bits poid fort du acc dans x23
	ldr    x23,[x19]              //Acc
	//Placement du pointeur x24 (i,0) formule a+(i·n1+j)·k
	madd   x24,x22,x28,x20        //x20+i*16
	//Charger les bits à x24 (i,0) dans x25
	ldr    x25,[x24]              //Mémoire

	//Chargement des 64 bits poid faible du acc dans x26
	ldr    x26,[x19,8]            //Acc
	//Charger les bits à x24 (i,1) dans x27
	ldr    x27,[x24,8]            //Mémoire

	//Soustraction mem[i] - acc
	subs   x26,x27,x26            //Soustraction bits poid faible
	sbcs   x23,x25,x23            //Soustraction bits poid fort + débordement

	str    x26,[x24,8]            //Stocker x26 dans mem[i]case 1
	str    x23,[x24]              //Stocker x23 dans mem[i]case 0

//Affichage de l'accumulateur
AffichageAcc:
	adr    x19,acc
	ldr    x22,[x19]              //Acc case (0)
	ldr    x23,[x19,8]            //Acc case (1)
	adr    x0,fmtAcc
	mov    x1,x22
	mov    x2,x23
	bl     printf

	mov    x25,0                  //Initialisation compteur à 0
	adr    x20,memoire
//Affichage de la mémoire
AffichageMem:
	ldr    x22,[x20]              //Mémoire case (i,0)
	ldr    x24,[x20,8]            //Mémoire case (i,1)

	adr    x0,fmtMem
	mov    x1,x25
	mov    x2,x22
	mov    x3,x24
	bl     printf

	add    x20,x20,16            //Déplacer x20 vers la prochaine ligne (i+1,j)
	add    x25,x25,1             //Compteur++
	cmp    x25,4                 //Si compteur != 4
	b.ne   AffichageMem          //Vrai:AffichageMem

	b      main                  //Recommencer depuis le début

Quitter:
    mov     x0, 0
    bl      exit

.section ".rodata"
fmtNum:     	.asciz  "%lu"
fmtAcc:         .asciz  "acc:    %016lX %016lX\n"
fmtMem:         .asciz  "mem[%lu]: %016lX %016lX\n"

.section ".bss"
num:       		.skip   8
.section ".data"
memoire:      	.xword  0, 0, 0, 0, 0, 0, 0, 0
acc:            .xword  0,0
